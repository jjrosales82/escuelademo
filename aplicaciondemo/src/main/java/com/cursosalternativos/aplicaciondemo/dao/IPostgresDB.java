package com.cursosalternativos.aplicaciondemo.dao;

import com.cursosalternativos.aplicaciondemo.xml.bean.Calificacion;
import com.cursosalternativos.aplicaciondemo.xml.bean.ListaCalificaciones;

public interface IPostgresDB {
	
	boolean registrarCalificacion(Calificacion calif);
	ListaCalificaciones obtenerCalificaciones(int idAlumno);
	boolean actualizarCalificacion(Calificacion calif);
	boolean eliminarCalificacion(Calificacion calif);

}
