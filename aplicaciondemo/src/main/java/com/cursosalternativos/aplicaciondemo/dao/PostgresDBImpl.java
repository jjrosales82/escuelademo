package com.cursosalternativos.aplicaciondemo.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;

import com.cursosalternativos.aplicaciondemo.xml.bean.Calificacion;
import com.cursosalternativos.aplicaciondemo.xml.bean.ListaCalificaciones;

public class PostgresDBImpl implements IPostgresDB {
	
	private static String sqlInsert = "INSERT INTO t_calificaciones VALUE (default,?,?,?,current_date)";
	private static String sqlUpdate = "UPDATE t_calificaciones SET calificacion = ? , fecha_registro = current_date WHERE id_t_materias = ? AND id_t_usuarios = ?";
	private static String sqlDelete = "DELETE t_calificaciones WHERE id_t_materias = ? AND id_t_usuarios = ? ";
	
	@Autowired
	@Qualifier("calificacionesJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * Registrar la calificación de un alumno especifico
	 * @author Javier Rosales
	 */
	@Override
	public boolean registrarCalificacion(Calificacion calif) {

		Object[] parametros = new Object[]{calif.getMateria(),calif.getUsuario(),calif.getCalificacion()};
		
		if( this.jdbcTemplate.update(sqlInsert, parametros) > 0)
			return true;
		else
			return false;
	
	}

	/**
	 * Obtener las calificaciones que tiene registradas un alumno
	 * @author Javier Rosales
	 */
	@Override
	public ListaCalificaciones obtenerCalificaciones(int idAlumno) {

		return null;
	}

	/**
	 * Actualizar la calificación de un alumno para una materia especifica
	 * @author Javier Rosales
	 */
	@Override
	public boolean actualizarCalificacion(Calificacion calif) {

		Object[] parametros = new Object[]{calif.getCalificacion(),calif.getMateria(),calif.getUsuario()};
		
		if( this.jdbcTemplate.update(sqlUpdate, parametros) > 0)
			return true;
		else
			return false;

	}

	/**
	 * Eliminar una calificación específica para un alumno determinado
	 * @author Javier Rosales
	 */
	@Override
	public boolean eliminarCalificacion(Calificacion calif) {

		Object[] parametros = new Object[]{calif.getMateria(),calif.getUsuario()};
		
		if( this.jdbcTemplate.update(sqlDelete, parametros) > 0)
			return true;
		else
			return false;

	}

}
