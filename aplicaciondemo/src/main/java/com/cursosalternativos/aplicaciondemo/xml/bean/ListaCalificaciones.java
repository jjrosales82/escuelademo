package com.cursosalternativos.aplicaciondemo.xml.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "calificacion"
})
@XmlRootElement(name = "ListaCalificaciones")
public class ListaCalificaciones {

    @XmlElement(name = "calificacion")
    protected List<Calificacion> calificacion;
    
    public List<Calificacion> getCalificacion() {
        if (calificacion == null) {
        	calificacion = new ArrayList<Calificacion>();
        }
        return this.calificacion;
    }
}
