package com.cursosalternativos.aplicaciondemo.xml.json.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.cursosalternativos.aplicaciondemo.dao.IPostgresDB;
import com.cursosalternativos.aplicaciondemo.xml.bean.Calificacion;
import com.cursosalternativos.aplicaciondemo.xml.bean.ListaCalificaciones;

public class EscuelaServiceImpl implements IEscuelaService {

	@Autowired
	private IPostgresDB postgresdb;
	
	public IPostgresDB getPostgresdb() {
		return postgresdb;
	}

	public void setPostgresdb(IPostgresDB postgresdb) {
		this.postgresdb = postgresdb;
	}

	@Override
	/**
	 * Registrar la calificaci�n de un alumno.
	 * @author Javier Rosales
	 * 
	 */
	public String registrarCalificacion(Calificacion calificacion) {
		
		String respuesta = null;
		
		if(this.postgresdb.registrarCalificacion(calificacion))
			respuesta = "La calificaci�n fue registrada de forma exitosa.";
		else 
			respuesta = "No fu� posible registrar la calificaci�n solicitada.";
		
		return respuesta;
	}

	/**
	 * Obtener el listado de calificaciones de un alumno.
	 * @author Javier Rosales
	 */
	@Override
	public ListaCalificaciones calificaciones(int identificadorAlumno) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Se actualiza la calificacion de un alumno
	 * @author Javier Rosales
	 */
	@Override
	public String actualizarCalificacion(Calificacion calificacion) {
		String respuesta = null;
		
		if(this.postgresdb.actualizarCalificacion(calificacion))
			respuesta = "La calificaci�n fue actualizada de forma exitosa.";
		else 
			respuesta = "No fu� posible actualizar la calificaci�n solicitada.";
		
		return respuesta;
	}

	/**
	 * Se elimina la calificacion de un alumno
	 * @author Javier Rosales
	 */
	@Override
	public String eliminarCalificacion(Calificacion calificacion) {
		String respuesta = null;
		
		if(this.postgresdb.eliminarCalificacion(calificacion))
			respuesta = "La calificaci�n fue eliminada de forma exitosa.";
		else 
			respuesta = "No fu� posible eliminar la calificaci�n solicitada.";
		
		return respuesta;
	}

}
