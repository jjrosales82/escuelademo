package com.cursosalternativos.aplicaciondemo.xml.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
 

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "materia",
    "usuario",
    "calificacion"
})

@XmlRootElement(name = "Calificacion")
public class Calificacion {
	
	private int materia;
	private int usuario;
	private float calificacion;
	//private String fechaRegistro;
	
	public int getMateria() {
		return materia;
	}
	public void setMateria(int materia) {
		this.materia = materia;
	}
	public int getUsuario() {
		return usuario;
	}
	public void setUsuario(int usuario) {
		this.usuario = usuario;
	}
	public float getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(float calificacion) {
		this.calificacion = calificacion;
	}

}
