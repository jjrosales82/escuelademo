package com.cursosalternativos.aplicaciondemo.xml.json.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cursosalternativos.aplicaciondemo.xml.bean.Calificacion;
import com.cursosalternativos.aplicaciondemo.xml.bean.ListaCalificaciones;

@Path("/escuelaService")
public interface IEscuelaService {
	
	// "http://localhost:8080/EscuelaDemo/services/escuelaService/registrarCalificacion"
	@POST
	@Path("registrarCalificacion")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_FORM_URLENCODED})
	public String registrarCalificacion(Calificacion calificacion);
	
	// "http://localhost:8080/EscuelaDemo/services/escuelaService/calificaciones/id"
	@GET
	@Path("calificaciones/{id}")
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public ListaCalificaciones calificaciones(@PathParam("id") int identificadorAlumno);
	
	// "http://localhost:8080/EscuelaDemo/services/escuelaService/actualizarCalificacion"
	@PUT
	@Path("actualizarCalificacion")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_FORM_URLENCODED})
	public String actualizarCalificacion(Calificacion calificacion);
	
	// "http://localhost:8080/EscuelaDemo/services/escuelaService/eliminarCalificacion"
    @DELETE
    @Path("eliminarCalificacion")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,})
    @Produces({MediaType.APPLICATION_FORM_URLENCODED})
    public String eliminarCalificacion(Calificacion calificacion);
	
	
}
