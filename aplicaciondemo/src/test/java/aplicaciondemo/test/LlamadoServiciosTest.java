package aplicaciondemo.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class LlamadoServiciosTest {
	
	
	
	@DisplayName("Se registra calificación")
	@Test
	void testRegistrarCalificacion() {
		 String requestURL = "http://localhost:8080/EscuelaDemo/services/escuelaService/registrarCalificacion";
	        String httpMethod = HttpMethod.POST;
	        String contentType = MediaType.APPLICATION_FORM_URLENCODED;
	        String accept = MediaType.APPLICATION_JSON;
	        String requestString =  "{"
	                +       "\"materia\": 1,"
	                +       "\"usuario\": 2,"
	                +       "\"calificacion\": 9.5"
	                +   "}";
	 
	        String[] requestParams = null;
	        requestParams = new String[]{requestURL, httpMethod, contentType, accept, requestString};
	 
	        String responseFromService = testRegistroService(requestParams);
	        System.out.println("respuesta: " + responseFromService);
	    
	}

	public String testRegistroService(String[] requestParams) {
		 
        URL url = null;
        HttpURLConnection httpURLConnection = null;
        String responseXML = null; 
 
        try {
            url = new URL(requestParams[0]);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod(requestParams[1]);
            httpURLConnection.setRequestProperty("Content-Type", requestParams[2]);
            httpURLConnection.setRequestProperty("Accept", requestParams[3]);
            httpURLConnection.setDoOutput(true);
 
            if (httpURLConnection.getResponseCode() == 200) {
                responseXML = getRespuestaJSON(httpURLConnection);
            }
        }
        catch(Exception  ex){
            ex.printStackTrace();
        }
        finally{
            httpURLConnection.disconnect();
        }
        return responseXML;
    }
	
    private static String getRespuestaJSON(HttpURLConnection httpURLConnection) throws IOException{
    	 
        StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = null;
        InputStreamReader inputStreamReader = null;
        String readSingleLine = null;
 
        try{

            inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
            bufferedReader = new BufferedReader(inputStreamReader);

            while ((readSingleLine = bufferedReader.readLine()) != null) {
                stringBuffer.append(readSingleLine);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally{
            bufferedReader.close();
            httpURLConnection.disconnect();
        }
        return stringBuffer.toString();
    }
	
}
